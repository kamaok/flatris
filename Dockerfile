FROM node

ARG APP_DIR=app

RUN mkdir -p ${APP_DIR}

WORKDIR ${APP_DIR}

COPY package.json ./

RUN yarn install

COPY ./ .

RUN yarn build

EXPOSE 3000

CMD ["yarn", "start"]
